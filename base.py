from discord.ext import commands
import logging
import discord
import asyncio

logger = logging.getLogger("useless")

class Bot(commands.Bot):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


    async def start(self, *args, **kwargs):
        self.invite_url = (
            "https://discordapp.com/api/oauth2/authorize?"
            "client_id=594176945386815492&permissions=0&scope=bot"
        )
        self.git_url = "https://gitlab.com/rock500/discord-hackweek-bot"
        self.logger = logger

        await super().start(*args, **kwargs)