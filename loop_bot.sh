#!/bin/bash

trap "break" SIGTERM SIGINT SIGKILL
while (true); do
    $HOME/code/discord-hackweek-bot/.venv/bin/python $HOME/code/discord-hackweek-bot/bot.py
    sleep 1
done
trap SIGTERM SIGINT SIGKILL -
