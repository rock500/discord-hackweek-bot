from discord.ext import commands
from base import Bot
import discord
import asyncio
import logging

bot = Bot(
    command_prefix="u.",
    owner_id=293567313985273856,
    description="""Useless""",
    debug = True,
)

@bot.listen()
async def on_ready():
    await bot.change_presence(
        activity=discord.Activity(type=3, name="Useless")
    )

@bot.listen()
async def on_message(message):
    ctx = await bot.get_context(message)
    if ctx.message.content in ["u.git", "u.help", "u.invite", "u.shutdown"]:
        return
    elif ctx.message.content.startswith("u."):
        await ctx.send("shutting down")
        await asyncio.sleep(1)
        await bot.logout()

@bot.command()
async def git(ctx):
    await ctx.send(bot.git_url)

@bot.command()
async def invite(ctx):
    await ctx.send(bot.invite_url)

@bot.command()
async def shutdown(ctx):
    await ctx.send("shutting down")
    await bot.logout()

with open("../useless_bot_token.txt") as fp:
    token = fp.read().strip()
bot.run(token)